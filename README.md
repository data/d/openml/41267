# OpenML dataset: particulate-matter-ukair-2017

https://www.openml.org/d/41267

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Hourly particulate matter air polution data of Great Britain for the year 2017, provided by Ricardo Energy and Environment on behalf of the UK Department for Environment, Food and Rural Affairs (DEFRA) and the Devolved Administrations on [https://uk-air.defra.gov.uk/]. The data was scraped from the UK AIR homepage via the R-package 'rdefra' [Vitolo, C., Russell, A., & Tucker, A. (2016, August). Rdefra: interact with the UK AIR pollution database from DEFRA. The Journal of Open Source Software, 1(4). doi:10.21105/joss.00051] on 09.11.2018. The data was published by DEFRA under the Open Government Licence (OGL) [http://www.nationalarchives.gov.uk/doc/open-government-licence/version/2/]. For a description of all variables, checkout the UK AIR homepage. The variable 'PM.sub.10..sub..particulate.matter..Hourly.measured.' was chosen as the target. The dataset also contains another measure of particulate matter 'PM.sub.2.5..sub..particulate.matter..Hourly.measured.' (ignored by default) which could be used as the target instead. The string variable 'datetime' (ignored by default) could be used to construct additional date/time features.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41267) of an [OpenML dataset](https://www.openml.org/d/41267). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41267/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41267/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41267/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

